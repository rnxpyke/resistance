# First build frontend with npm
FROM node:16 as frontend

WORKDIR /resistance/frontend

COPY ./frontend .

RUN npm install
RUN PUBLIC_URL=/app npm run build

# Rust as the base image
FROM rust:latest as backend

WORKDIR /resistance/backend

COPY --from=frontend /resistance/frontend/build /resistance/frontend/build
COPY ./backend .

RUN cargo build --release --features STATIC_FRONTEND

WORKDIR /resistance
RUN mv ./backend/target/release/resistance .
RUN rm -rf frontend/ backend/

Expose 3001

CMD ["/resistance/resistance", "-s", "0.0.0.0:3001"]

mod game;
mod game_utils;
mod static_frontend;

use game_utils::{LobbyId, UserId};

use game::{
    LobbyIn,
    LobbyOut,
    LobbyMessage,
    GameMsgIn,
    GameLobby,
    GameError,
    run_lobby,
    play_game,
};

use axum::{
    extract::{
        ws::{Message, WebSocket, WebSocketUpgrade},
        Path,
    },
    http::{HeaderValue, Method, StatusCode},
    response::{IntoResponse, Json, Redirect},
    routing::{get, post},
    Extension, Router,
};
use futures_util::stream::StreamExt;
use once_cell::sync::OnceCell;
use rand::{distributions::Alphanumeric, Rng};

use serde_json::{json, Value};
use std::net::SocketAddr;
use std::sync::Arc;
use tokio::sync::{mpsc, RwLock};
use tower_cookies::{CookieManagerLayer, Cookies, Key};
use tower_http::cors::CorsLayer;
use std::collections::BTreeMap;
use static_frontend::static_path;


use argh::FromArgs;

async fn hello_world() -> &'static str {
    "Hello World!"
}


struct AppState {
    lobbies: RwLock<BTreeMap<LobbyId, LobbyIn>>,
}

async fn run_game(
    state: Arc<AppState>,
    lobby_id: LobbyId,
    mut lobby: GameLobby,
    mut receiver: LobbyOut,
) {
    tracing::debug!("Start handling lobby");
    match run_lobby(lobby_id.clone(), &mut lobby, &mut receiver).await {
        Ok(_) => {},
        Err(GameError::AllClosed) => {
            tracing::debug!("Lobby {:?} closed", &lobby_id);
            state.lobbies.write().await.remove(&lobby_id);
            receiver.close();
            return;
        }
    }

    tracing::debug!("Lobby {:?} is running", &lobby_id);
    play_game(&mut lobby, &mut receiver).await;

    tracing::debug!("Lobby {:?} finished", &lobby_id);
    state.lobbies.write().await.remove(&lobby_id);
    receiver.close();
    return;
}

async fn websocket(stream: WebSocket, lobby_id: LobbyId, user_id: UserId, game_channel: LobbyIn) {
    // By splitting we can send and receive at the same time.
    tracing::debug!(
        "Splitting websocket stream lobby {:?}, user {:?}",
        &lobby_id,
        &user_id
    );
    let (sender, mut receiver) = stream.split();

    tracing::debug!(
        "Insert websocket sink {:?} into lobby {:?}",
        &user_id,
        &lobby_id
    );

    game_channel
        .send(LobbyMessage::Connected {
            user: user_id.clone(),
            sink: sender,
        })
        .await
        .unwrap();

    tracing::debug!("start forwarding messages");
    while let Some(Ok(Message::Text(text))) = receiver.next().await {
        tracing::debug!("forward mesage: {}", &text);
        let msg: Result<GameMsgIn, _> = serde_json::from_str(&text);
        match msg {
            Ok(message) => {
                tracing::debug!("sending {:?}", &message);
                game_channel
                    .send(LobbyMessage::Game {
                        user: user_id.clone(),
                        message,
                    })
                    .await
                    .unwrap();
            }
            Err(e) => {
                tracing::debug!("could not deserialize: {} {:?}", &text, e)
            }
        }
    }

    tracing::debug!("channel closed, lobby {:?}, user {:?}", &lobby_id, &user_id);

    game_channel
        .send(LobbyMessage::Disconnected {
            user: user_id.clone(),
        })
        .await
        .unwrap();
}

async fn websocket_handler(
    ws: WebSocketUpgrade,
    Extension(state): Extension<Arc<AppState>>,
    Path(lobby_id): Path<String>,
    cookies: Cookies,
) -> Result<impl IntoResponse, StatusCode> {
    let key = KEY.get().unwrap();
    let private_cookies = cookies.private(key);

    let rstring: String = rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(7)
        .map(char::from)
        .collect();
    let new_user_id = UserId(rstring);

    tracing::debug!("private token cookie: {:?}", private_cookies.get("token"));
    let user_id = private_cookies
        .get("token")
        .and_then(|c| c.value().parse().ok().map(UserId))
        .unwrap_or(new_user_id);

    // private_cookies.add(Cookie::new("token", user_id.0.clone()));

    let lobby = {
        let lobbies = state.lobbies.read().await;
        match lobbies.get(&LobbyId(lobby_id.clone())) {
            Some(lobby) => lobby.clone(),
            None => {
                tracing::debug!("Lobby with id {:?} does not exist", &lobby_id);
                return Err(StatusCode::BAD_REQUEST);
            }
        }
    };

    tracing::debug!("upgrading websocket");
    Ok(ws.on_upgrade(move |socket| websocket(socket, LobbyId(lobby_id), user_id, lobby)))
}



async fn create_lobby(
    Extension(state): Extension<Arc<AppState>>,
) -> Result<Json<Value>, StatusCode> {
    let (sender, reciever) = mpsc::channel(20);
    let rstring: String = rand::thread_rng()
        .sample_iter(&Alphanumeric)
        .take(7)
        .map(char::from)
        .collect();
    let lobby_id = LobbyId(rstring);
    let lobby = GameLobby::default();
    {
        let mut lobbies = state.lobbies.write().await;
        if lobbies.contains_key(&lobby_id) {
            tracing::debug!("lobby with id {:?} already exists", lobby_id);
            return Err(StatusCode::INTERNAL_SERVER_ERROR);
        }
        lobbies.insert(lobby_id.clone(), sender);
        tracing::debug!("lobby count {}", &lobbies.len());
    }
    tokio::spawn(run_game(state.clone(), lobby_id.clone(), lobby, reciever));
    Ok(Json(json!({"lobby_id": lobby_id.0.clone() })))
}

static KEY: OnceCell<Key> = OnceCell::new();

#[derive(FromArgs)]
/// A simple resistance server
struct Args {
    /// the socket to listen on
    #[argh(option, short = 's', default = "SocketAddr::from(([127, 0, 0, 1], 3001))")]
    socket: SocketAddr,

    /// the cors origin to use
    #[argh(option, short = 'o', default = "\"http://localhost:3001\".to_owned()")]
    cors_origin: String,
}

#[tokio::main]
async fn main() {
    // initialize tracing
    tracing_subscriber::fmt::init();

    let my_key: &[u8] = &[0; 64]; // Your real key must be cryptographically random
    KEY.set(Key::from(my_key)).ok();

    let args: Args = argh::from_env();

    let state = Arc::new(AppState {
        lobbies: RwLock::new(BTreeMap::default()),
    });

    let app = Router::new()
        .route("/", get(|| async { Redirect::permanent("/app") }))
        .route("/hello-world", get(hello_world))
        .route("/create_lobby", post(create_lobby))
        .layer(
            CorsLayer::new()
                .allow_origin(args.cors_origin.parse::<HeaderValue>().unwrap())
                .allow_methods([Method::GET]),
        )
        .layer(Extension(state.clone()))
        .route("/ws/:lobby_id", get(websocket_handler))
        .layer(Extension(state.clone()))
        .layer(CookieManagerLayer::new())
        .route("/app/*path", get(static_path));

    // run our app with hyper
    // `axum::Server` is a re-export of `hyper::Server`
    tracing::debug!("listening on {}", &args.socket);
    axum::Server::bind(&args.socket)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

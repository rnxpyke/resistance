#[cfg(feature = "STATIC_FRONTEND")]
use include_dir::{include_dir, Dir};

#[cfg(feature = "STATIC_FRONTEND")]
use axum::{
    http::{HeaderValue, header},
    body::Full,
};

use axum::{
    extract::Path,
    http::StatusCode,
    response::{IntoResponse, Response},
    body,
    body::Empty,
};



#[cfg(feature = "STATIC_FRONTEND")]
static STATIC_DIR: Dir<'_> = include_dir!("$CARGO_MANIFEST_DIR/../frontend/build");

#[cfg(not(feature = "STATIC_FRONTEND"))]
pub async fn static_path(Path(_path): Path<String>) -> impl IntoResponse {
    Response::builder()
    .status(StatusCode::NOT_IMPLEMENTED)
    .body(body::boxed(Empty::new()))
    .unwrap();
}

#[cfg(feature = "STATIC_FRONTEND")]
pub async fn static_path(Path(path): Path<String>) -> impl IntoResponse {
    let path_str = path.trim_start_matches('/');
    let path_buf: std::path::PathBuf = path_str.to_owned().into();
    let mime_type = mime_guess::from_path(path).first_or_text_plain();

    if let Some(file) = STATIC_DIR.get_file(&path_buf) {
        return Response::builder()
            .status(StatusCode::OK)
            .header(
                header::CONTENT_TYPE,
                HeaderValue::from_str(mime_type.as_ref()).unwrap(),
            )
            .body(body::boxed(Full::from(file.contents())))
            .unwrap();
    }

    let index_path = path_buf.join("index.html");
    let mime_type = mime_guess::from_path(&index_path).first_or_text_plain();
    match STATIC_DIR.get_file(index_path) {
        None => Response::builder()
            .status(StatusCode::NOT_FOUND)
            .body(body::boxed(Empty::new()))
            .unwrap(),
        Some(file) => Response::builder()
        .status(StatusCode::OK)
        .header(
            header::CONTENT_TYPE,
            HeaderValue::from_str(mime_type.as_ref()).unwrap(),
        )
        .body(body::boxed(Full::from(file.contents())))
        .unwrap()
    }
}
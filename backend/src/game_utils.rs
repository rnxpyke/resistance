
use serde_derive::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialOrd, Ord, PartialEq, Eq, Serialize, Deserialize, Hash)]
pub(crate) struct UserId(pub String);

#[derive(Clone, Debug, PartialOrd, Ord, PartialEq, Eq)]
pub(crate) struct LobbyId(pub String);

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub(crate) enum Role {
    Spy,
    Resistance,
}

#[derive(Copy, Clone, Debug)]
pub(crate) struct RoundMeta<T>(T, T, T, T, T);

impl<T> RoundMeta<T> {
    pub fn nth(&self, i: usize) -> Option<&T> {
        match i {
            1 => Some(&self.0),
            2 => Some(&self.1),
            3 => Some(&self.2),
            4 => Some(&self.3),
            5 => Some(&self.4),
            _ => None,
        }
    }
}

impl<T> From<RoundMeta<T>> for Vec<T> {
    fn from(meta: RoundMeta<T>) -> Self {
        vec![meta.0, meta.1, meta.2, meta.3, meta.4]
    }
}

impl Default for RoundMeta<u8> {
    fn default() -> Self {
        RoundMeta(2, 3, 2, 3, 3)
    }
}

#[derive(Copy, Clone, Debug)]
pub(crate) enum RoundT<P, F> {
    Round0(F, F, F, F, F),
    Round1(P, F, F, F, F),
    Round2(P, P, F, F, F),
    Round3(P, P, P, F, F),
    Round4(P, P, P, P, F),
    Round5(P, P, P, P, P),
}

impl<P, F> From<RoundT<P, F>> for Vec<P> {
    fn from(rounds: RoundT<P, F>) -> Self {
        use RoundT::*;
        match rounds {
            Round0(_, _, _, _, _) => vec![],
            Round1(p1, _, _, _, _) => vec![p1],
            Round2(p1, p2, _, _, _) => vec![p1, p2],
            Round3(p1, p2, p3, _, _) => vec![p1, p2, p3],
            Round4(p1, p2, p3, p4, _) => vec![p1, p2, p3, p4],
            Round5(p1, p2, p3, p4, p5) => vec![p1, p2, p3, p4, p5],
        }
    }
}

impl<P, F> RoundT<P, F> {
    pub fn rounds_played(&self) -> usize {
        use RoundT::*;
        match self {
            Round0(_, _, _, _, _) => 0,
            Round1(_, _, _, _, _) => 1,
            Round2(_, _, _, _, _) => 2,
            Round3(_, _, _, _, _) => 3,
            Round4(_, _, _, _, _) => 4,
            Round5(_, _, _, _, _) => 5,
        }
    }
    pub fn push(self, p: P) -> Self {
        use RoundT::*;
        match self {
            Round0(_, f2, f3, f4, f5) => Round1(p, f2, f3, f4, f5),
            Round1(p1, _, f2, f3, f4) => Round2(p1, p, f2, f3, f4),
            Round2(p1, p2, _, f3, f4) => Round3(p1, p2, p, f3, f4),
            Round3(p1, p2, p3, _, f4) => Round4(p1, p2, p3, p, f4),
            Round4(p1, p2, p3, p4, _) => Round5(p1, p2, p3, p4, p),
            Round5(p1, p2, p3, p4, p5) => Round5(p1, p2, p3, p4, p5),
        }
    }
}

impl<F> RoundT<bool, F> {
    pub fn winner(&self) -> Option<bool> {
        use RoundT::*;
        let true_count: u8 = match *self {
            Round0(_, _, _, _, _) => 0,
            Round1(a, _, _, _, _) => [a].iter().filter(|&x| *x).count() as u8,
            Round2(a, b, _, _, _) => [a, b].iter().filter(|&x| *x).count() as u8,
            Round3(a, b, c, _, _) => [a, b, c].iter().filter(|&x| *x).count() as u8,
            Round4(a, b, c, d, _) => [a, b, c, d].iter().filter(|&x| *x).count() as u8,
            Round5(a, b, c, d, e) => [a, b, c, d, e].iter().filter(|&x| *x).count() as u8,
        };
        let false_coount: u8 = match *self {
            Round0(_, _, _, _, _) => 0,
            Round1(a, _, _, _, _) => [a].iter().filter(|&x| !*x).count() as u8,
            Round2(a, b, _, _, _) => [a, b].iter().filter(|&x| !*x).count() as u8,
            Round3(a, b, c, _, _) => [a, b, c].iter().filter(|&x| !x).count() as u8,
            Round4(a, b, c, d, _) => [a, b, c, d].iter().filter(|&x| !x).count() as u8,
            Round5(a, b, c, d, e) => [a, b, c, d, e].iter().filter(|&x| !x).count() as u8,
        };
        if true_count >= 3 {
            Some(true)
        } else if false_coount >= 3 {
            Some(false)
        } else {
            None
        }
    }
}

impl<P, F> Default for RoundT<P, F>
where
    F: Default,
{
    fn default() -> Self {
        RoundT::Round0(
            F::default(),
            F::default(),
            F::default(),
            F::default(),
            F::default(),
        )
    }
}

use axum::extract::ws::{Message, WebSocket};
use futures_util::sink::SinkExt;
use futures_util::stream::SplitSink;
use rand::seq::SliceRandom;
use serde_derive::{Deserialize, Serialize};
use std::collections::{BTreeMap, HashSet};
use std::error::Error;
use tokio::sync::mpsc;
use tokio::time::Duration;

pub(crate) type LobbyIn = mpsc::Sender<LobbyMessage>;
pub(crate) type LobbyOut = mpsc::Receiver<LobbyMessage>;

use crate::game_utils::*;


#[derive(Debug)]
pub(crate) enum LobbyMessage {
    Game { user: UserId, message: GameMsgIn },
    Connected { user: UserId, sink: SplitSink<WebSocket, Message> },
    Disconnected { user: UserId },
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
#[serde(tag = "type", content = "value")]
pub(crate) enum GameMsgIn {
    SetName(String),
    ChooseTeam(HashSet<UserId>),
    SetVote(Option<bool>),
    SetMissionVote(Option<bool>),
    SetLeader(Option<UserId>),
    StartGame,
    VoteNow,
    MissionNow,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
#[serde(tag = "type")]
pub(crate) enum GameMsgOut {
    StartRound { round_results: Vec<bool>, required_participants: u8 },
    ChooseTeam { leader: UserId, vote: u8, timeout_secs: u64 },
    VoteStart { participants: HashSet<UserId>, timeout_secs: u64 },
    VoteStatus { votes: BTreeMap<UserId, Option<bool>> },
    VoteResult { votes: BTreeMap<UserId, Option<bool>>, result: bool },
    MissionStart { participants: HashSet<UserId>, timeout_secs: u64 },
    MissionVoteStatus { votes: BTreeMap<UserId, Option<bool>> },
    MissionResult { mission_votes: Vec<bool>, result: bool },
    SelectedTeam { team: HashSet<UserId> },
    ConnectedUsers { users: BTreeMap<UserId, String> },
    YourId { id: UserId },
    StartGame {
        known_roles: BTreeMap<UserId, Role>,
        leader: UserId,
        players: HashSet<UserId>,
        required_participants: Vec<u8>,
    },
}

#[derive(Clone, Debug, Default)]
pub(crate) struct GameState {
    pub roles: BTreeMap<UserId, Role>,
    pub round_results: RoundT<bool, ()>,
    pub participants: HashSet<UserId>,
    pub votes: BTreeMap<UserId, Option<bool>>,
    pub mission_votes: BTreeMap<UserId, Option<bool>>,
    pub leader: Option<UserId>,
    pub round_participants: RoundMeta<u8>,
}

#[derive(Debug, Default)]
pub(crate) struct GameLobby {
    pub names: BTreeMap<UserId, String>,
    pub game: GameState,
    pub out_sinks: BTreeMap<UserId, SplitSink<WebSocket, Message>>,
}

impl GameLobby {
    async fn send_all_clients(&mut self, msg: &GameMsgOut) {
        let out_sinks = &mut self.out_sinks;
        let text = serde_json::to_string(msg).unwrap();
        for (_, sink) in out_sinks.iter_mut() {
            sink.send(Message::Text(text.clone())).await.unwrap();
        }
    }

    async fn start_game(&mut self) -> Result<bool, Box<dyn Error>> {
        if self.game.participants.len() != 5 {
            return Ok(false);
        }
        let (participants, leader, spies, agents) = {
            let mut rng = rand::thread_rng();
            let mut participants: Vec<UserId> = self.game.participants.iter().cloned().collect();
            participants.shuffle(&mut rng);
            let leader: UserId = self
                .game
                .leader
                .insert(participants.choose(&mut rng).unwrap().clone())
                .clone();
            let (spies, agents) = participants.split_at(2);
            let owned_spies = spies.to_owned();
            let owned_agents = agents.to_owned();
            (participants, leader, owned_spies, owned_agents)
        };
        let spies_iter = spies.iter().cloned().map(|x| (x, Role::Spy));
        let agents_iter = agents.iter().cloned().map(|x| (x, Role::Resistance));
        self.game.roles = spies_iter.chain(agents_iter).collect();

        for agent in &agents {
            let sink = self.out_sinks.get_mut(agent).ok_or("no outsink")?;
            let mut known_roles = BTreeMap::new();
            known_roles.insert(agent.clone(), Role::Resistance);
            let msg = GameMsgOut::StartGame {
                known_roles,
                leader: leader.clone(),
                players: participants.iter().cloned().collect(),
                required_participants: self.game.round_participants.into(),
            };
            let msg_text = serde_json::to_string(&msg).unwrap();
            sink.send(Message::Text(msg_text)).await?;
        }

        for spy in &spies {
            let sink = self.out_sinks.get_mut(spy).ok_or("no outsink")?;
            let known_roles: BTreeMap<UserId, Role> =
                spies.iter().map(|x| (x.clone(), Role::Spy)).collect();
            let msg = GameMsgOut::StartGame {
                known_roles,
                leader: leader.clone(),
                players: participants.iter().cloned().collect(),
                required_participants: self.game.round_participants.into(),
            };
            let msg_text = serde_json::to_string(&msg).unwrap();
            sink.send(Message::Text(msg_text)).await?;
        }

        Ok(true)
    }

    fn connected_lobby_users(&mut self) -> BTreeMap<UserId, String> {
        let mut users = BTreeMap::new();
        for user in self.out_sinks.keys() {
            let name = self
                .names
                .entry(user.clone())
                .or_insert_with(|| format!("Agent {}", rand::random::<u16>()));
            users.insert(user.clone(), name.clone());
        }

        return users;
    }

    async fn update_vote_status(&mut self) {
        let has_voted: BTreeMap<UserId, Option<bool>> = self
            .game
            .votes
            .iter()
            .filter(|(_, &val)| val != None)
            .map(|(id, _)| (id.clone(), None))
            .collect();

        for (id, sink) in self.out_sinks.iter_mut() {
            let mut votes = has_voted.clone();
            let own_vote = self.game.votes.get(id).copied().flatten();
            if own_vote != None {
                votes.insert(id.clone(), own_vote);
            }
            let message = GameMsgOut::VoteStatus { votes };
            let message_text = serde_json::to_string(&message).unwrap();
            sink.send(Message::Text(message_text)).await.unwrap();
        }
    }

    async fn update_mission_vote_status(&mut self) {
        let has_voted: BTreeMap<UserId, Option<bool>> = self
            .game
            .mission_votes
            .iter()
            .filter(|(key,_)| self.game.participants.contains(key))
            .filter(|(_, &val)| val != None)
            .map(|(id, _)| (id.clone(), None))
            .collect();

        for (id, sink) in self.out_sinks.iter_mut() {
            let votes = match self.game.participants.contains(id) {
                true => {
                    let mut votes = has_voted.clone();
                    let own_vote_opt = self.game.mission_votes.get(id).copied().flatten();
                    if let Some(own_vote) = own_vote_opt {
                        votes.insert(id.clone(), Some(own_vote));
                    }
                    votes
                },
                false => { has_voted.clone() }
            };

            let msg = GameMsgOut::MissionVoteStatus { votes };
            let msg_text = serde_json::to_string(&msg).unwrap();
            sink.send(Message::Text(msg_text)).await.unwrap();
        }
    }

    async fn add_client(&mut self, user: UserId, mut sink: SplitSink<WebSocket, Message>) {
        let id = serde_json::to_string(&GameMsgOut::YourId { id: user.clone() }).unwrap();
        if let Err(err) = sink.send(Message::Text(id)).await {
            tracing::debug!("error sending to {:?}, {:?}", &user, &err);
        };
        let old = self.out_sinks.insert(user.clone(), sink);
        if let Some(mut old_sender) = old {
            old_sender.close().await.unwrap();
        }
        self.names
            .entry(user.clone())
            .or_insert_with(|| format!("Agent {}", rand::random::<u16>()));
 
        let users = self.connected_lobby_users();
        self.send_all_clients(&GameMsgOut::ConnectedUsers { users }).await;

        self.send_all_clients(&GameMsgOut::SelectedTeam {
            team: self.game.participants.clone(),
        })
        .await;
    }

    async fn reconnect_client(&mut self, user: UserId, mut sink: SplitSink<WebSocket, Message>) {
        if let None = self.game.roles.get(&user) {
            return;
        }
        let id = serde_json::to_string(&GameMsgOut::YourId { id: user.clone() }).unwrap();
        if let Err(err) = sink.send(Message::Text(id)).await {
            tracing::debug!("error sending to {:?}, {:?}", &user, &err);
        };
        let old = self.out_sinks.insert(user.clone(), sink);
        if let Some(mut old_sender) = old {
            old_sender.close().await.unwrap();
        }
    }
}


pub(crate) enum GameError {
    AllClosed,
}

pub(crate) async fn run_lobby(
    lobby_id: LobbyId,
    lobby: &mut GameLobby,
    receiver: &mut LobbyOut,
) -> Result<(), GameError> {
    while let Some(message) = receiver.recv().await {
        use GameMsgIn::*;
        use LobbyMessage::*;
        match message {
            Connected { user, sink } => {
                tracing::debug!("Inserting user {:?} into lobby {:?}", &user, &lobby_id);
                lobby.add_client(user.clone(), sink).await;
                let users = lobby.connected_lobby_users();
                lobby
                    .send_all_clients(&GameMsgOut::ConnectedUsers { users })
                    .await;
                lobby.game.participants.insert(user.clone());
                lobby
                    .send_all_clients(&GameMsgOut::SelectedTeam { team: lobby.game.participants.clone() })
                    .await;
            }
            Disconnected { user } => {
                tracing::debug!("Removing user {:?} from lobby {:?}", &user, &lobby_id);
                lobby.game.participants.remove(&user);
                let old = lobby.out_sinks.remove(&user);
                if let Some(mut old_sender) = old {
                    old_sender.close().await.unwrap();
                }
                let users = lobby.connected_lobby_users();
                lobby
                    .send_all_clients(&GameMsgOut::ConnectedUsers { users })
                    .await;
                lobby
                    .send_all_clients(&GameMsgOut::SelectedTeam {
                        team: lobby.game.participants.clone(),
                    })
                    .await;
            }
            Game {
                user,
                message: ChooseTeam(team),
            } => {
                tracing::debug!("User {:?} chose team {:?}", &user, &team);
                lobby.game.participants = team
                    .into_iter()
                    .filter(|user| lobby.out_sinks.contains_key(user))
                    .collect();
                lobby
                    .send_all_clients(&GameMsgOut::SelectedTeam {
                        team: lobby.game.participants.clone(),
                    })
                    .await;
            }
            Game {
                user,
                message: StartGame,
            } => {
                tracing::debug!("User {:?} wants to start the game", &user);
                let res = lobby.start_game().await.unwrap();
                if res {
                    return Ok(());
                }
            }
            Game {
                user,
                message: SetName(name),
            } => {
                tracing::debug!("User {:?} wants to set name {:?}", &user, &name);
                lobby.names.insert(user.clone(), name);
                let users = lobby.connected_lobby_users();
                lobby
                    .send_all_clients(&GameMsgOut::ConnectedUsers { users })
                    .await;
            }
            _ => {}
        }
    }

    return Err(GameError::AllClosed);
}

#[derive(Copy,Clone, Debug)]
enum IdleError {
    AllClosed,
}

async fn idle(
    duration: Duration,
    lobby: &mut GameLobby,
    receiver: &mut mpsc::Receiver<LobbyMessage>,
) -> Result<(), IdleError>{
    let fut = async {
        while let Some(message) = receiver.recv().await {
            use LobbyMessage::*;
            match message {
                Connected { user, sink } => {
                    lobby.reconnect_client(user.clone(), sink).await;
                }
                Disconnected { user } => {
                    let old = lobby.out_sinks.remove(&user);
                    if let Some(mut old_sender) = old {
                        old_sender.close().await.unwrap();
                    }
                }
                _ => {}
            }
        }
        let res: Result<(), IdleError> = Err(IdleError::AllClosed);
        res
    };
    match tokio::time::timeout(duration, fut).await {
        Ok(Err(e)) => Err(e),
        _ => Ok(())
    }
}

pub(crate) async fn play_game(
    lobby: &mut GameLobby,
    receiver: &mut mpsc::Receiver<LobbyMessage>,
) {
    for round in 1..=5 {
        tracing::debug!("Starting round {}", round);
        lobby.game.mission_votes = BTreeMap::new();
        lobby.game.votes = BTreeMap::new();
        lobby
            .send_all_clients(&GameMsgOut::StartRound {
                round_results: lobby.game.round_results.into(),
                required_participants: *lobby.game.round_participants.nth(round).unwrap(),
            })
            .await;
        let mut mission_result = false;
        for vote_round in 1..=5 {
            tracing::debug!("Starting vote round {}", vote_round);
            lobby.game.votes = BTreeMap::new();
            let leader = {
                let mut players: Vec<_> = lobby.game.roles.keys().cloned().collect();
                let mut rng = rand::thread_rng();
                players.shuffle(&mut rng);
                let leader: UserId = players.choose(&mut rng).unwrap().clone();
                lobby.game.leader.insert(leader).clone()
            };
            lobby
                .send_all_clients(&GameMsgOut::ChooseTeam { leader, vote: vote_round, timeout_secs: 10*60 })
                .await;
            let team = match choose_team(lobby, receiver).await {
                Ok(team) => team,
                Err(ChooseTeamError::NoTeamChosen) => {
                    tracing::debug!(
                        "Error in vote round {}: {:?}",
                        vote_round,
                        ChooseTeamError::NoTeamChosen
                    );
                    continue;
                }
                Err(ChooseTeamError::AllClosed) => {
                    tracing::debug!(
                        "Error in vote round {}: {:?}",
                        vote_round,
                        ChooseTeamError::AllClosed
                    );
                    return;
                }
            };
            lobby.game.participants = team;
            lobby
                .send_all_clients(&GameMsgOut::VoteStart {
                    participants: lobby.game.participants.clone(),
                    timeout_secs: 10,
                })
                .await;
            let vote_res = match vote_on_team(lobby, receiver).await {
                Ok(vote) => vote,
                Err(e) => {
                    tracing::debug!("Error in vote round {}: {:?}", vote_round, e);
                    return;
                }
            };

            lobby
                .send_all_clients(&GameMsgOut::VoteResult {
                    votes: lobby.game.votes.clone(),
                    result: vote_res,
                })
                .await;

            if let Err(_) = idle(Duration::from_secs(5), lobby, receiver).await {
                return;
            }

            if vote_res {
                tracing::debug!("Voting round {} finished", vote_round);
                lobby
                    .send_all_clients(&GameMsgOut::MissionStart {
                        participants: lobby.game.participants.clone(),
                        timeout_secs: 10,
                    })
                    .await;
                match execute_mission(lobby, receiver).await {
                    Ok(result) => mission_result = result,
                    Err(e) => {
                        tracing::debug!("Error in mission round {}: {:?}", vote_round, &e);
                        return;
                    }
                }
                lobby
                    .send_all_clients(&GameMsgOut::MissionResult {
                        mission_votes: lobby.game.mission_votes.values().cloned().filter_map(|x| x).collect(),
                        result: mission_result,
                    })
                    .await;
                if let Err(_) = idle(Duration::from_secs(3), lobby, receiver).await {
                    return;
                }
                break;
            }
        }
        lobby.game.round_results = lobby.game.round_results.push(mission_result);
        if let Some(_winner) = lobby.game.round_results.winner() {
            tracing::debug!("Game finished");
            break;
        }
    }

    // Announce the winner
}

#[derive(Copy, Clone, Debug)]
enum MissionError {
    AllClosed,
}

async fn execute_mission(
    lobby: &mut GameLobby,
    receiver: &mut mpsc::Receiver<LobbyMessage>,
) -> Result<bool, MissionError> {
    let vote_fut = async {
        while let Some(message) = receiver.recv().await {
            use GameMsgIn::*;
            use LobbyMessage::*;
            match message {
                Connected { user, sink } => {
                    lobby.reconnect_client(user.clone(), sink).await;
                }
                Disconnected { user } => {
                    let old = lobby.out_sinks.remove(&user);
                    if let Some(mut old_sender) = old {
                        old_sender.close().await.unwrap();
                    }
                }
                Game {
                    user,
                    message: SetVote(vote),
                } => {
                    if !lobby.game.participants.contains(&user) {
                        continue;
                    }
                    lobby.game.mission_votes.insert(user.clone(), vote);
                    if let None = vote {
                        lobby.game.mission_votes.remove(&user);
                    }
                    lobby.update_mission_vote_status().await;
                }
                _ => {}
            }
        }
        let res: Result<(), MissionError> = Err(MissionError::AllClosed);
        res
    };
    match tokio::time::timeout(Duration::from_secs(10), vote_fut).await {
        Ok(Err(e)) => Err(e),
        _ => {
            let pro_votes = lobby
                .game
                .mission_votes
                .values()
                .filter(|&&vote| vote == Some(true))
                .count();
            if pro_votes < lobby.game.participants.len() {
                Ok(false)
            } else {
                Ok(true)
            }
        }
    }
}

#[derive(Copy, Clone, Debug)]
enum VoteTeamError {
    AllClosed,
}

async fn vote_on_team(
    lobby: &mut GameLobby,
    receiver: &mut mpsc::Receiver<LobbyMessage>,
) -> Result<bool, VoteTeamError> {
    let vote_fut = async {
        while let Some(message) = receiver.recv().await {
            use GameMsgIn::*;
            use LobbyMessage::*;
            match message {
                Connected { user, sink } => {
                    lobby.reconnect_client(user.clone(), sink).await;
                }
                Disconnected { user } => {
                    let old = lobby.out_sinks.remove(&user);
                    if let Some(mut old_sender) = old {
                        old_sender.close().await.unwrap();
                    }
                }
                Game {
                    user,
                    message: SetVote(vote),
                } => {
                    lobby.game.votes.insert(user.clone(), vote);
                    if let None = vote {
                        lobby.game.votes.remove(&user);
                    }
                    lobby.update_vote_status().await;
                }
                _ => {}
            }
        }
        let res: Result<(), VoteTeamError> = Err(VoteTeamError::AllClosed);
        res
    };
    match tokio::time::timeout(Duration::from_secs(10), vote_fut).await {
        Ok(Err(e)) => Err(e),
        _ => {
            let pro_votes = lobby
                .game
                .votes
                .values()
                .filter(|&&vote| vote == Some(true))
                .count();
            let con_votes = lobby.game.roles.len() - pro_votes;
            if pro_votes > con_votes {
                Ok(true)
            } else {
                Ok(false)
            }
        }
    }
}

#[derive(Copy, Clone, Debug)]
enum ChooseTeamError {
    AllClosed,
    NoTeamChosen,
}

async fn choose_team(
    lobby: &mut GameLobby,
    receiver: &mut mpsc::Receiver<LobbyMessage>,
) -> Result<HashSet<UserId>, ChooseTeamError> {
    let res = async {
        let mut result = Err(ChooseTeamError::AllClosed);
        while let Some(message) = receiver.recv().await {
            use GameMsgIn::*;
            use LobbyMessage::*;
            match message {
                Connected { user, sink } => {
                    lobby.reconnect_client(user.clone(), sink).await;
                }
                Disconnected { user } => {
                    let old = lobby.out_sinks.remove(&user);
                    if let Some(mut old_sender) = old {
                        old_sender.close().await.unwrap();
                    }
                }
                Game {
                    user,
                    message: ChooseTeam(team),
                } => {
                    tracing::debug!("User {:?} chose team {:?}", &user, &team);
                    if &user != lobby.game.leader.get_or_insert(user.clone()) {
                        continue;
                    }
                    lobby.game.participants = team
                        .into_iter()
                        .filter(|user| lobby.out_sinks.contains_key(user))
                        .collect();
                    lobby
                        .send_all_clients(&GameMsgOut::SelectedTeam {
                            team: lobby.game.participants.clone(),
                        })
                        .await;
                }
                Game {
                    user,
                    message: VoteNow,
                } => {
                    tracing::debug!("User {:?} wants to start the vote", &user);
                    if Some(&user) != lobby.game.leader.as_ref() {
                        tracing::debug!("User {:?} is not the leader", &user);
                        continue;
                    }
                    let participants = lobby.game.participants.len();
                    let round = lobby.game.round_results.rounds_played() + 1;
                    let required_participants = lobby
                        .game
                        .round_participants
                        .nth(round)
                        .map(|&n| n as usize);
                    if Some(participants) != required_participants {
                        tracing::debug!(
                            "Not enough participants: {}/{:?}",
                            participants,
                            required_participants
                        );
                        continue;
                    }
                    result = Ok(());
                    break;
                }
                _ => {}
            }
        }
        result
    };
    match tokio::time::timeout(Duration::from_secs(10 * 60), res).await {
        Ok(Ok(())) => {}
        Ok(Err(e)) => {
            return Err(e);
        }
        Err(_) => {
            let participants = lobby.game.participants.len();
            let round = lobby.game.round_results.rounds_played() + 1;
            let required_participants = lobby
                .game
                .round_participants
                .nth(round)
                .map(|&n| n as usize);
            if Some(participants) != required_participants {
                tracing::debug!(
                    "Timeout: Not enough participants: {}/{:?}",
                    participants,
                    required_participants
                );
                return Err(ChooseTeamError::NoTeamChosen);
            }
        }
    }
    return Ok(lobby.game.participants.clone());
}
